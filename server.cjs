const http = require('http')
const fs = require('fs')
const uuid = require('uuid')
const path = require('path')

http.createServer(function (req, res) {
    const statusCode = req.url.split('/')[2];
    const delaySecond = req.url.split('/')[2];
    if (req.url === '/html' && req.method === 'GET') {
        let filePath = path.join(__dirname, "index.html");
        fs.readFile(filePath, 'utf-8', (error, data) => {
            if (error) {
                res.writeHead(500, { 'Content-Type': 'application/json' });
                const errorMessage = {
                    message: 'Facing problem in reading html file'
                }
                res.write(JSON.stringify(errorMessage));
                res.end();
            }
            else {
                res.writeHead(200, { 'Content-type': 'text/html' });
                res.write(data);
                res.end();
            }
        })
    }
    else if (req.url === '/json' && req.method === "GET") {
        let filePath = path.join(__dirname, "data.json");
        fs.readFile(filePath, 'utf-8', (error, data) => {
            if (error) {
                res.writeHead(500, { 'Content-Type': 'application/json' });
                const errorMessage = {
                    message: 'Facing problem in reading json file'
                }
                res.write(JSON.stringify(errorMessage));
                res.end();
            }
            else {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.write(data);
                res.end();
            }
        })
    }
    else if (req.url === '/uuid' && req.method === "GET") {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        const uuidObject = {
            uuid: uuid.v4()
        };
        res.write(JSON.stringify(uuidObject));
        res.end();
    }
    else if (req.url === `/status/${statusCode}` && req.method === 'GET') {
        if (!isNaN(statusCode) && http.STATUS_CODES[statusCode]) {
            res.writeHead(statusCode, { 'Content-Type': 'application/json' });
            const statusMessage = {
                status: `${statusCode} ${http.STATUS_CODES[statusCode]}`
            };
            res.write(JSON.stringify(statusMessage));
            res.end();
        }
        else {
            res.writeHead(400, { 'Content-Type': 'application/json' })
            const errorMessage = {
                message: "Status code is not valid"
            }
            res.write(JSON.stringify(errorMessage));
            res.end();
        }
    }
    else if (req.url === `/delay/${delaySecond}` && req.method === 'GET') {
        let second = delaySecond;
        if (isNaN(second)) {
            res.writeHead(400, { 'Content-Type': 'application/json' });
            const errorMessage = {
                message: "Second is not a number"
            }
            res.write(JSON.stringify(errorMessage));
            res.end();
        }
        else {
            setTimeout(() => {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                const delayTimes = {
                    'delayInseconds': second
                };
                res.write(JSON.stringify(delayTimes));
                res.end();
            }, second * 1000);
        }
    }
    else {
        res.writeHead(404, { 'Content-Type': 'application/json' });
        const errorMessage = {
            massage: "You have entered wrong url"
        }
        res.write(JSON.stringify(errorMessage));
        res.end()
    }
}).listen(8080, () => {
    console.log("Server is running on port 8080");
});
